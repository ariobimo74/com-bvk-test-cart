package com.bvk.test.cart.model.dto;

import java.util.List;

public class ResponseListDto
{
    private int acknowledge;
    private String message;
    private List<?> dataList;

    public ResponseListDto(int acknowledge, String message, List<?> dataList) {
        this.acknowledge = acknowledge;
        this.message = message;
        this.dataList = dataList;
    }

    public int getAcknowledge() {
        return acknowledge;
    }

    public void setAcknowledge(int acknowledge) {
        this.acknowledge = acknowledge;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<?> getDataList() {
        return dataList;
    }

    public void setDataList(List<?> dataList) {
        this.dataList = dataList;
    }
}
