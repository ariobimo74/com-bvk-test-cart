package com.bvk.test.cart.model.dto;

import java.util.Map;
import java.util.Optional;

public class ResponseDataDto
{
    private int acknowledge;
    private String message;
    private Optional<?> data;

    public ResponseDataDto() {
    }

    public ResponseDataDto(int acknowledge, String message, Optional<?> data) {
        this.acknowledge = acknowledge;
        this.message = message;
        this.data = data;
    }

    public int getAcknowledge() {
        return acknowledge;
    }

    public void setAcknowledge(int acknowledge) {
        this.acknowledge = acknowledge;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Optional<?> getData() {
        return data;
    }

    public void setData(Optional<?> data) {
        this.data = data;
    }
}
