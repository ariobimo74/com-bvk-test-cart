package com.bvk.test.cart.repository;

import com.bvk.test.cart.model.dto.ProductBrandDto;
import com.bvk.test.cart.model.entity.ProductBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductBrandRepository extends JpaRepository<ProductBrand, Long>
{
    @Query(nativeQuery = true)
    List<ProductBrandDto> getAllProductBrand(@Param("brandName") String brandName);
}
