package com.bvk.test.cart.service.impl;

import com.bvk.test.cart.model.dto.ProductDto;
import com.bvk.test.cart.model.entity.Product;
import com.bvk.test.cart.model.entity.ProductBrand;
import com.bvk.test.cart.repository.ProductRepository;
import com.bvk.test.cart.service.ProductBrandService;
import com.bvk.test.cart.service.ProductService;
import com.bvk.test.cart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService
{
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private ProductBrandService productBrandService;

    public List<ProductDto> getAll()
    {
        return productRepository.getAllProducts();
    }

    public List<ProductDto> searchProductDto(String productName, String brandName, long typeId)
    {
        if (typeId > 0)
        {
            return productRepository.searchByNameBrandCategory(productName, brandName, typeId);
        }

        return productRepository.searchByNameBrand(productName, brandName);
    }

    public Optional<Product> getProductOpt(long id)
    {
        return productRepository.findById(id);
    }

    public ProductDto getProductDto(long id)
    {
        Product product = productRepository.getById(id);
        ProductBrand productBrand = productBrandService.getProductBrand(product.getProductBrandId());
        String typeName = productTypeService.getProductType(productBrand.getProductTypeId()).getTypeName();

        return new ProductDto(product.getId(), product.getProductName(), productBrand.getBrandName(), typeName, product.getPrice());
    }
}
