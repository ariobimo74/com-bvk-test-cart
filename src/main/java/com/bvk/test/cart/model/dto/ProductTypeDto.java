package com.bvk.test.cart.model.dto;

public class ProductTypeDto
{
    private long id;
    private String productTypeName;

    public ProductTypeDto() {
    }

    public ProductTypeDto(long id, String productTypeName) {
        this.id = id;
        this.productTypeName = productTypeName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }
}
