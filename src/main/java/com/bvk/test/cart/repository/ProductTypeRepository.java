package com.bvk.test.cart.repository;

import com.bvk.test.cart.model.dto.ProductTypeDto;
import com.bvk.test.cart.model.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductTypeRepository extends JpaRepository<ProductType, Long>
{
    @Query(nativeQuery = true)
    public List<ProductTypeDto> getAllProductType();
}
