package com.bvk.test.cart.controller;

import com.bvk.test.cart.model.Constants;
import com.bvk.test.cart.model.dto.ResponseListDto;
import com.bvk.test.cart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;

@RestController
@RequestMapping(path = "types")
public class ProductTypeRest
{
    @Autowired
    private ProductTypeService productTypeService;

    @GetMapping
    public ResponseEntity<?> getAll()
    {
        try
        {
            if (productTypeService.productTypeDtoDropDown().isEmpty()) {
                return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA,
                        productTypeService.productTypeDtoDropDown()), HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS,
                    productTypeService.productTypeDtoDropDown()), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, e.getMessage(), new ArrayList<>()),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
