package com.bvk.test.cart.model.entity;

import com.bvk.test.cart.model.CommonEntity;
import com.bvk.test.cart.model.dto.CartTransactionDto;

import javax.persistence.*;
import java.util.Date;

@SqlResultSetMapping(name = "cartMapping", classes = {@ConstructorResult(targetClass = CartTransactionDto.class, columns = {
        @ColumnResult(name = "id", type = Long.class),
        @ColumnResult(name = "productName", type = String.class),
        @ColumnResult(name = "brand", type = String.class),
        @ColumnResult(name = "type", type = String.class),
        @ColumnResult(name = "price", type = String.class),
        @ColumnResult(name = "addedToChartDate", type = Date.class)
})})

@NamedNativeQuery(name = "CartTransaction.getByUserId",
        query = "SELECT cart_transaction.id, product.product_name AS productName, product_brand.brand_name AS brand, product_type.type_name AS type, product.price, cart_transaction.created_on AS addedToChartDate " +
                "FROM product " +
                "LEFT JOIN product_brand ON product.product_brand_id = product_brand.id " +
                "LEFT JOIN product_type ON product_brand.product_type_id = product_type.id " +
                "WHERE cart_transaction.isDelete = false " +
                "AND cart_transaction.modified_by =:userId")

@Entity
@Table(name = "cart_transaction")
public class CartTransaction extends CommonEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "product_id")
    private long productId;

    public CartTransaction() {
    }

    public CartTransaction(long id) {
        this.id = id;
    }

    public CartTransaction(long id, long productId) {
        this.id = id;
        this.productId = productId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
