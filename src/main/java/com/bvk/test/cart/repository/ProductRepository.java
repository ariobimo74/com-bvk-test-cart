package com.bvk.test.cart.repository;

import com.bvk.test.cart.model.dto.ProductDto;
import com.bvk.test.cart.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>
{
    @Query(nativeQuery = true)
    List<ProductDto> getAllProducts();

    @Query(nativeQuery = true)
    List<ProductDto> searchByNameBrandCategory(@Param("productName") String productName, @Param("brandName") String brandName,
                                               @Param("typeId") long typeId);

    @Query(nativeQuery = true)
    List<ProductDto> searchByNameBrand(@Param("productName") String productName, @Param("brandName") String brandName);
}
