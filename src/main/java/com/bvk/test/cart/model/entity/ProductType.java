package com.bvk.test.cart.model.entity;

import com.bvk.test.cart.model.CommonEntity;
import com.bvk.test.cart.model.dto.ProductTypeDto;

import javax.persistence.*;

@SqlResultSetMapping(name = "productTypeDropDown", classes = {@ConstructorResult(targetClass = ProductTypeDto.class, columns = {
        @ColumnResult(name = "id", type = Long.class),
        @ColumnResult(name = "productTypeName", type = String.class),
})})

@NamedNativeQuery(name = "ProductType.getAllProductType",
        query = "SELECT id, type_name AS productTypeName FROM product_type WHERE isDelete = false ORDER BY type_name",
        resultSetMapping = "productTypeDropDown")

@Entity
@Table(name = "product_type")
public class ProductType extends CommonEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "type_name", nullable = false)
    private String typeName;

    public ProductType() {
    }

    public ProductType(Long id, String typeName) {
        this.id = id;
        this.typeName = typeName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
