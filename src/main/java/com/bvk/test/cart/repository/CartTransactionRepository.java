package com.bvk.test.cart.repository;

import com.bvk.test.cart.model.dto.CartTransactionDto;
import com.bvk.test.cart.model.entity.CartTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartTransactionRepository extends JpaRepository<CartTransaction, Long>
{
    @Query(nativeQuery = true)
    List<CartTransactionDto> getByUserId(@Param("userId") long userId);
}
