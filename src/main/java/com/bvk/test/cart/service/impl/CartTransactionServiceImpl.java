package com.bvk.test.cart.service.impl;

import com.bvk.test.cart.model.dto.CartTransactionDto;
import com.bvk.test.cart.model.dto.RequestAddCartDto;
import com.bvk.test.cart.model.dto.RequestAddCartsDto;
import com.bvk.test.cart.model.entity.CartTransaction;
import com.bvk.test.cart.repository.CartTransactionRepository;
import com.bvk.test.cart.service.CartTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CartTransactionServiceImpl implements CartTransactionService
{
    @Autowired
    private CartTransactionRepository cartTransactionRepository;

    public List<CartTransaction> getAll()
    {
        return cartTransactionRepository.findAll();
    }

    public List<CartTransactionDto> getAll(long userId)
    {
        return cartTransactionRepository.getByUserId(userId);
    }

    public Optional<CartTransaction> getCartTransaction(long id)
    {
        return cartTransactionRepository.findById(id);
    }

    public CartTransaction addToCart(RequestAddCartDto requestAddCartDto)
    {
        CartTransaction cartTransaction = createCartTransaction();

        cartTransaction.setProductId(requestAddCartDto.getProductId());
        cartTransaction.setCreatedBy(requestAddCartDto.getUserId());
        cartTransaction.setCreatedOn(new Date());
        cartTransaction.setDelete(false);

        return cartTransactionRepository.save(cartTransaction);
    }

    public List<CartTransaction> addToCart(RequestAddCartsDto requestAddCartsDto)
    {
        long[] productIds = requestAddCartsDto.getProductsId();
        List<CartTransaction> cartTransactions = new ArrayList<>();

        for (long productId : productIds)
        {
            cartTransactions.add(addToCart(new RequestAddCartDto(productId, requestAddCartsDto.getUserId())));
        }

        return cartTransactions;
    }

    public CartTransaction hideCartTransaction(long userId, long id)
    {
        CartTransaction cartTransaction = cartTransactionRepository.getById(id);

        cartTransaction.setDelete(true);
        cartTransaction.setDeletedOn(new Date());
        cartTransaction.setDeletedBy(userId);

        return cartTransaction;
    }

    public double totalPrice(long userId)
    {
        return getAll(userId).stream().map(CartTransactionDto::getPrice).mapToDouble(Double::parseDouble).sum();
    }

    private CartTransaction createCartTransaction()
    {
        if (getAll().isEmpty())
        {
            return new CartTransaction();
        }

        long id = getAll().stream().map(CartTransaction::getId).max(Comparator.comparing(Long::valueOf))
                .orElse(Long.parseLong("1"));

        return new CartTransaction(id);
    }
}
