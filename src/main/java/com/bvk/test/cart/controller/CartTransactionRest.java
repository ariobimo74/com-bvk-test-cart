package com.bvk.test.cart.controller;

import com.bvk.test.cart.model.Constants;
import com.bvk.test.cart.model.dto.CartTransactionDto;
import com.bvk.test.cart.model.dto.RequestAddCartDto;
import com.bvk.test.cart.model.dto.ResponseDataDto;
import com.bvk.test.cart.model.dto.ResponseListDto;
import com.bvk.test.cart.model.entity.CartTransaction;
import com.bvk.test.cart.service.CartTransactionService;
import com.bvk.test.cart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping(path = "/carts")
public class CartTransactionRest
{
    @Autowired
    private CartTransactionService cartTransactionService;

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<?> getAllCart(Principal principal)
    {
        List<CartTransactionDto> cartTransactions = cartTransactionService.getAll(Long.parseLong(principal.getName()));

        try
        {
            long userId = Long.parseLong(principal.getName());
        }
        catch (NullPointerException e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, Constants.MESSAGE_SAVE_FAILED_USER, new ArrayList<>()),
                    HttpStatus.UNAUTHORIZED);
        }

        if (cartTransactions.isEmpty())
        {
            return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA, cartTransactions),
                    HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS, cartTransactions),
                HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<?> saveProduct(@RequestBody(required = true) RequestAddCartDto requestAddCartDto, Principal principal)
    {
        long userId;
        Map<String, String> errorMessage = new HashMap<>();

        try
        {
            userId = Long.parseLong(principal.getName());
            requestAddCartDto.setUserId(userId);
        }
        catch (NullPointerException e)
        {
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, e.getMessage());
            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_SAVE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.UNAUTHORIZED);
        }

        if (!productService.getProductOpt(requestAddCartDto.getProductId()).isPresent())
        {
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, Constants.MESSAGE_SAVE_FAILED_PRODUCT + requestAddCartDto.getProductId());
            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_SAVE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.NOT_FOUND);
        }

        try {
            Map<String, CartTransaction> responseDataDtoMap = new HashMap<>();
            responseDataDtoMap.put(Constants.DATA_PARAM, cartTransactionService.addToCart(requestAddCartDto));

            return new ResponseEntity<>(new ResponseDataDto(1, Constants.MESSAGE_SAVE_SUCCESS,
                    Optional.of(responseDataDtoMap)), HttpStatus.OK);
        }
        catch (Exception e)
        {
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, e.getMessage());
            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_SAVE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> hideCartTransaction(@PathVariable("id") long id, Principal principal)
    {
        Map<String, String> errorMessage = new HashMap<>();
        long userId;

        try {
            userId = Long.parseLong(principal.getName());
        }
        catch (NullPointerException e)
        {
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, e.getMessage());

            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_DELETE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.UNAUTHORIZED);
        }

        if (cartTransactionService.getCartTransaction(id).isPresent())
        {
            Map<String, CartTransaction> responseDataDtoMap = new HashMap<>();
            responseDataDtoMap.put(Constants.DATA_PARAM, cartTransactionService.hideCartTransaction(userId, id));

            return new ResponseEntity<>(new ResponseDataDto(1, Constants.MESSAGE_SAVE_SUCCESS,
                    Optional.of(responseDataDtoMap)), HttpStatus.OK);
        }
        else
        {
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, Constants.MESSAGE_DELETE_FAILED_CART + id);

            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_DELETE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/total")
    public ResponseEntity<?> getTotalPrice(Principal principal)
    {
        try
        {
            Map<String, Double> totalPrice = new HashMap<>();
            totalPrice.put(Constants.DATA_PARAM, cartTransactionService.totalPrice(Long.parseLong(principal.getName())));

            if (cartTransactionService.getAll(Long.parseLong(principal.getName())).isEmpty())
            {
                return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA, new ArrayList<>()),
                        HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(new ResponseDataDto(1, Constants.MESSAGE_RETRIEVE_PRICE_SUCCESS,
                    Optional.of(totalPrice)), HttpStatus.OK);
        }
        catch (NullPointerException e)
        {
            Map<String, String> errorMessage = new HashMap<>();
            errorMessage.put(Constants.ERROR_CAUSE_PARAM, e.getMessage());

            return new ResponseEntity<>(new ResponseDataDto(0, Constants.MESSAGE_DELETE_FAILED,
                    Optional.of(errorMessage)), HttpStatus.UNAUTHORIZED);
        }
    }
}
