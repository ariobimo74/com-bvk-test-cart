package com.bvk.test.cart.model.dto;

public class RequestAddCartDto
{
    private long productId;
    private long userId;

    public RequestAddCartDto() {
    }

    public RequestAddCartDto(long productId, long userId) {
        this.productId = productId;
        this.userId = userId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
