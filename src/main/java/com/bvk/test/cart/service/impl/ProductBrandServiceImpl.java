package com.bvk.test.cart.service.impl;

import com.bvk.test.cart.model.dto.ProductBrandDto;
import com.bvk.test.cart.model.entity.ProductBrand;
import com.bvk.test.cart.repository.ProductBrandRepository;
import com.bvk.test.cart.service.ProductBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProductBrandServiceImpl implements ProductBrandService
{
    @Autowired
    private ProductBrandRepository productBrandRepository;

    public List<ProductBrandDto> getAllProductBrand(String brandName)
    {
        return productBrandRepository.getAllProductBrand(brandName);
    }

    public ProductBrand getProductBrand(long id)
    {
        return productBrandRepository.findById(id).get();
    }
}
