package com.bvk.test.cart.model.entity;

import com.bvk.test.cart.model.CommonEntity;
import com.bvk.test.cart.model.dto.ProductDto;

import javax.persistence.*;

@SqlResultSetMapping(name = "productMapping", classes = {@ConstructorResult(targetClass = ProductDto.class, columns = {
        @ColumnResult(name = "id", type = Long.class),
        @ColumnResult(name = "productName", type = String.class),
        @ColumnResult(name = "brand", type = String.class),
        @ColumnResult(name = "type", type = String.class),
        @ColumnResult(name = "price", type = String.class)
})})

@NamedNativeQuery(name = "Product.getAllProducts",
        query = "SELECT product.id, product.product_name AS productName, product_brand.brand_name AS brand, product_type.type_name AS type, product.price " +
                "FROM product " +
                "LEFT JOIN product_brand ON product.product_brand_id = product_brand.id " +
                "LEFT JOIN product_type ON product_brand.product_type_id = product_type.id " +
                "WHERE product.isdelete = false " +
                "ORDER BY product.product_name",
        resultSetMapping = "productMapping")

@NamedNativeQuery(name = "Product.searchByNameBrandCategory",
        query = "SELECT product.id, product.product_name AS productName, product_brand.brand_name AS brand, product_type.type_name AS type, product.price " +
                "FROM product " +
                "LEFT JOIN product_brand ON product.product_brand_id = product_brand.id " +
                "LEFT JOIN product_type ON product_brand.product_type_id = product_type.id " +
                "WHERE product.isdelete = false " +
                "AND product.product_name ILIKE CONCAT('%',:productName,'%') " +
                "AND product_brand.brand_name ILIKE CONCAT('%',:brandName,'%') " +
                "AND product_type.id =:typeId " +
                "ORDER BY product.product_name",
        resultSetMapping = "productMapping")

@NamedNativeQuery(name = "Product.searchByNameBrand",
        query = "SELECT product.id, product.product_name AS productName, product_brand.brand_name AS brand, product_type.type_name AS type, product.price " +
                "FROM product " +
                "LEFT JOIN product_brand ON product.product_brand_id = product_brand.id " +
                "LEFT JOIN product_type ON product_brand.product_type_id = product_type.id " +
                "WHERE product.isdelete = false " +
                "AND product.product_name ILIKE CONCAT('%',:productName,'%') " +
                "AND product_brand.brand_name ILIKE CONCAT('%',:brandName,'%') " +
                "ORDER BY product.product_name",
        resultSetMapping = "productMapping")

@Entity
@Table(name = "product")
public class Product extends CommonEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "product_name", nullable = false)
    private String productName;

    @Column(name = "product_brand_id", nullable = false)
    private long productBrandId;

    @Column(name = "price", nullable = true)
    private String price;

    public Product() {
    }

    public Product(Long id, String productName, long productBrandId, String price) {
        this.id = id;
        this.productName = productName;
        this.productBrandId = productBrandId;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public long getProductBrandId() {
        return productBrandId;
    }

    public void setProductBrandId(long productBrandId) {
        this.productBrandId = productBrandId;
    }
}
