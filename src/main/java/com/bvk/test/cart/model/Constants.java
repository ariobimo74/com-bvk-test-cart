package com.bvk.test.cart.model;

public class Constants
{
    public static final String MESSAGE_RETRIEVE_NO_DATA = "Successfully retrieve data";

    public static final String MESSAGE_RETRIEVE_DATA_SUCCESS = "Successfully retrieve data";
    public static final String MESSAGE_SAVE_SUCCESS = "Successfully retrieve data";
    public static final String MESSAGE_RETRIEVE_PRICE_SUCCESS = "Successfully retrieve price";

    public static final String MESSAGE_SAVE_FAILED = "Failed save data";
    public static final String MESSAGE_DELETE_FAILED = "Failed delete data";
    public static final String MESSAGE_SAVE_FAILED_PRODUCT = "No such product id with primary key ";
    public static final String MESSAGE_SAVE_FAILED_USER = "No such user id with primary key ";
    public static final String MESSAGE_DELETE_FAILED_CART = "No such cart id with primary key ";

    public static final String DATA_PARAM = "dataParam";

    public static final String ERROR_CAUSE_PARAM = "errorCause";
}
