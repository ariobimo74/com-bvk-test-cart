package com.bvk.test.cart.service;

import com.bvk.test.cart.model.dto.ProductTypeDto;
import com.bvk.test.cart.model.entity.ProductType;

import java.util.List;

public interface ProductTypeService
{
    List<ProductTypeDto> productTypeDtoDropDown();

    public ProductType getProductType(long id);
}
