package com.bvk.test.cart.service;

import com.bvk.test.cart.model.dto.ProductDto;
import com.bvk.test.cart.model.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService
{
    public List<ProductDto> getAll();

    public List<ProductDto> searchProductDto(String productName, String brandName, long typeId);

    public Optional<Product> getProductOpt(long id);

    public ProductDto getProductDto(long id);
}
