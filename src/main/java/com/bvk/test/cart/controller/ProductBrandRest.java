package com.bvk.test.cart.controller;

import com.bvk.test.cart.model.Constants;
import com.bvk.test.cart.model.dto.ResponseListDto;
import com.bvk.test.cart.service.ProductBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "brands")
public class ProductBrandRest
{
    @Autowired
    private ProductBrandService productBrandService;

    @GetMapping
    public ResponseEntity<?> getProductBrand(@RequestParam("brandName") String brandName)
    {
        try
        {
            return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS,
                    productBrandService.getAllProductBrand(brandName)), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, e.getMessage(), new ArrayList<>()),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
