package com.bvk.test.cart.controller;

import com.bvk.test.cart.model.Constants;
import com.bvk.test.cart.model.dto.*;
import com.bvk.test.cart.model.entity.Product;
import com.bvk.test.cart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(path = "products")
public class ProductRest
{
    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<?> getAllProducts()
    {
        try
        {
            List<ProductDto> products = productService.getAll();

            if (products.isEmpty())
            {
                return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS, products), HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA, products), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, e.getMessage(), new ArrayList<>()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/search")
    public ResponseEntity<?> getAllProducts(@RequestBody(required = false) RequestSearchDto requestSearchDto)
    {
        try
        {
            List<ProductDto> products = productService.searchProductDto(requestSearchDto.getProductName(), requestSearchDto.getBrandName(), requestSearchDto.getProductType());

            if (products.isEmpty()) {
                return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS, products), HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(new ResponseListDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA, products), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, e.getMessage(), new ArrayList<>()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getProduct(@PathVariable("id") long id)
    {
        try
        {
            Optional<Product> productOpt = productService.getProductOpt(id);

            if (productOpt.isPresent()) {
                Optional<ProductDto> productDto = Optional.ofNullable(productService.getProductDto(id));

                return new ResponseEntity<>(new ResponseDataDto(1, Constants.MESSAGE_RETRIEVE_DATA_SUCCESS, productDto),
                        HttpStatus.OK);
            }

            return new ResponseEntity<>(new ResponseDataDto(1, Constants.MESSAGE_RETRIEVE_NO_DATA, productOpt),
                    HttpStatus.NOT_FOUND);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(new ResponseListDto(0, e.getMessage(), new ArrayList<>()),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
