package com.bvk.test.cart.model.dto;

public class RequestSearchDto
{
    private String productName;
    private String brandName;
    private long productType;

    public RequestSearchDto() {
    }

    public RequestSearchDto(String productName, String brandName, long productType) {
        this.productName = productName;
        this.brandName = brandName;
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public long getProductType() {
        return productType;
    }

    public void setProductType(long productType) {
        this.productType = productType;
    }
}
