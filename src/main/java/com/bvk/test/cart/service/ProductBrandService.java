package com.bvk.test.cart.service;

import com.bvk.test.cart.model.dto.ProductBrandDto;
import com.bvk.test.cart.model.entity.ProductBrand;

import java.util.List;

public interface ProductBrandService
{
    public List<ProductBrandDto> getAllProductBrand(String brandName);

    public ProductBrand getProductBrand(long id);
}
