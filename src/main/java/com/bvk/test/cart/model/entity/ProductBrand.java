package com.bvk.test.cart.model.entity;

import com.bvk.test.cart.model.CommonEntity;
import com.bvk.test.cart.model.dto.ProductBrandDto;

import javax.persistence.*;

@SqlResultSetMapping(name = "autoCompleteBrand", classes = {@ConstructorResult(targetClass = ProductBrandDto.class, columns = {
        @ColumnResult(name = "productBrandName", type = String.class)
})})

@NamedNativeQuery(name = "ProductBrand.getAllProductBrand",
        query = "SELECT brand_name AS productBrandName FROM product_brand WHERE brand_name ILIKE CONCAT('%',:brandName,'%') AND isDelete = false " +
                "ORDER BY brand_name ")

@Entity
@Table(name = "product_brand")
public class ProductBrand extends CommonEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "brand_name", nullable = false)
    private String brandName;

    @Column(name = "product_type_id", nullable = false)
    private long productTypeId;

    public ProductBrand() {
    }

    public ProductBrand(Long id, String brandName, long productTypeId) {
        this.id = id;
        this.brandName = brandName;
        this.productTypeId = productTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(long productTypeId) {
        this.productTypeId = productTypeId;
    }
}
