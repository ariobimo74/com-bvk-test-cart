package com.bvk.test.cart.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class CartTransactionDto
{
    private long id;
    private String productName;
    private String brand;
    private String type;
    private String price;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+07:00")
    @Temporal(TemporalType.DATE)
    private Date addedToChartDate;

    public CartTransactionDto() {
    }

    public CartTransactionDto(long id, String productName, String brand, String type, String price, Date addedToChartDate) {
        this.id = id;
        this.productName = productName;
        this.brand = brand;
        this.type = type;
        this.price = price;
        this.addedToChartDate = addedToChartDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getAddedToChartDate() {
        return addedToChartDate;
    }

    public void setAddedToChartDate(Date addedToChartDate) {
        this.addedToChartDate = addedToChartDate;
    }
}
