package com.bvk.test.cart.model.dto;

public class ProductBrandDto
{
    private String productBrandName;

    public ProductBrandDto() {
    }

    public ProductBrandDto(String productBrandName) {
        this.productBrandName = productBrandName;
    }

    public String getProductBrandName() {
        return productBrandName;
    }

    public void setProductBrandName(String productBrandName) {
        this.productBrandName = productBrandName;
    }
}
