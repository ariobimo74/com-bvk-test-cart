package com.bvk.test.cart.service.impl;

import com.bvk.test.cart.model.dto.ProductTypeDto;
import com.bvk.test.cart.model.entity.ProductType;
import com.bvk.test.cart.repository.ProductTypeRepository;
import com.bvk.test.cart.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductTypeServiceImpl implements ProductTypeService
{
    @Autowired
    private ProductTypeRepository productTypeRepository;

    public List<ProductTypeDto> productTypeDtoDropDown()
    {
        return productTypeRepository.getAllProductType();
    }

    public ProductType getProductType(long id)
    {
        return productTypeRepository.findById(id).get();
    }
}
