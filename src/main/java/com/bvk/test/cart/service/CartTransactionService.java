package com.bvk.test.cart.service;

import com.bvk.test.cart.model.dto.CartTransactionDto;
import com.bvk.test.cart.model.dto.RequestAddCartDto;
import com.bvk.test.cart.model.dto.RequestAddCartsDto;
import com.bvk.test.cart.model.entity.CartTransaction;

import java.util.List;
import java.util.Optional;

public interface CartTransactionService
{
    public List<CartTransactionDto> getAll(long userId);

    public Optional<CartTransaction> getCartTransaction(long id);

    public CartTransaction addToCart(RequestAddCartDto requestAddCartDto);

    public List<CartTransaction> addToCart(RequestAddCartsDto requestAddCartsDto);

    public CartTransaction hideCartTransaction(long userId, long id);

    public double totalPrice(long userId);
}
