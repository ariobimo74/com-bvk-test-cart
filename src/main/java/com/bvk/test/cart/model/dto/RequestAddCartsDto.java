package com.bvk.test.cart.model.dto;

public class RequestAddCartsDto
{
    private long[] productsId;
    private long userId;

    public RequestAddCartsDto() {
    }

    public RequestAddCartsDto(long[] productsId, long userId) {
        this.productsId = productsId;
        this.userId = userId;
    }

    public long[] getProductsId() {
        return productsId;
    }

    public void setProductsId(long[] productsId) {
        this.productsId = productsId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
