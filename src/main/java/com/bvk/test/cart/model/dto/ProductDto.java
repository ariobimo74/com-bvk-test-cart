package com.bvk.test.cart.model.dto;

public class ProductDto
{
    private long id;
    private String productName;
    private String brand;
    private String type;
    private String price;

    public ProductDto() {
    }

    public ProductDto(long id, String productName, String brand, String type, String price) {
        this.id = id;
        this.productName = productName;
        this.brand = brand;
        this.type = type;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
