package com.bvk.test.cart.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "/")
public class AuthRest
{
    @GetMapping
    public String welcome()
    {
        return "Welcome to the jungle!";
    }

    @GetMapping(path = "user")
    public Principal user(Principal principal)
    {
        System.out.println("principal = " + principal.getName());
        return principal;
    }
}
